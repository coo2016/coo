

/**
 * Created by Aetheya on 2/05/2016.
 */
package com.lsinf1225.thomasberton.uclove.database;

import android.text.method.DateTimeKeyListener;

import java.util.ArrayList;

public class Message {

    //private variables
    String _loginE;
    String _loginR;
    String _dateM;
    String _content;

    // Empty constructor
    public Message(){
    }
    // Empty constructor
    public Message(String loginE, String loginR, String dateM, String content){
        this._loginE = loginE;
        this._loginR = loginR;
        this._dateM = dateM;
        this._content = content;
    }


    // getting ID
    public String get_loginR(){
        return this._loginR;
    }
    public String get_loginE(){
        return this._loginE;
    }
    public String get_dateM(){
        return this._dateM;
    }
    public String get_content(){
        return this._content;
    }
    // setting id

    public void set_loginR(String id){
        this._loginR = id;
    }
    public void set_loginE(String id){
        this._loginE = id;
    }
    public void set_dateM(String id){
        this._loginE = id;
    }
    public void set_content(String id){
        this._content = id;
    }


    public static ArrayList<Message> getAListOfMessages() {
        ArrayList<Message> listMess = new ArrayList<Message>();

        listMess.add(new Message("Nom1", "Nom2", "Date1", "Contenu1"));
        listMess.add(new Message("Nom3", "Nom4", "Date3", "Contenu3"));
        listMess.add(new Message("Nom5", "Nom6", "Date4", "Contenu4"));
        listMess.add(new Message("Nom7", "Nom8", "Date5", "Contenu5"));
        listMess.add(new Message("Nom9", "Nom10", "Date6", "Contenu6"));
        listMess.add(new Message("Nom11", "Nom12", "Date7", "Contenu7"));
        listMess.add(new Message("Nom12", "Nom13", "Date8", "Contenu8"));
        listMess.add(new Message("Nom14", "Nom15", "Date9", "Contenu9"));
        return listMess;
    }
}