package com.lsinf1225.thomasberton.uclove;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.Toast;

import com.lsinf1225.thomasberton.uclove.database.DatabaseHandler;
import com.lsinf1225.thomasberton.uclove.database.DateRDV;
import com.lsinf1225.thomasberton.uclove.database.FriendAdaptater;
import com.lsinf1225.thomasberton.uclove.database.Person;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by X on 20-04-16.
 */
public class ActivityMeet extends Activity {
    Button btn;
    int year_x, month_x, day_x;
    static final int DIALOG_ID = 0;
    DatabaseHandler db = new DatabaseHandler(ActivityMeet.this);
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.meet);
        showDialogOnButtonClick();



        Button rdv = (Button) findViewById(R.id.buttondeletemeet);
        rdv.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                Intent intent3 = new Intent(ActivityMeet.this,ActivityUpdateDispo.class);
                startActivity(intent3);
            }
        });


    }

    public void showDialogOnButtonClick() {

        btn = (Button) findViewById(R.id.buttoncalendar);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog(DIALOG_ID);

            }
        });

    }

    @Override
    protected Dialog onCreateDialog(int id) {
        if (id == DIALOG_ID) {
            return new DatePickerDialog(this, dpickerListner, year_x, month_x, day_x);
        }
        return null;


    }

    private DatePickerDialog.OnDateSetListener dpickerListner = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
            year_x = i;
            month_x = i1;
            day_x = i2;
            StringBuffer sb = new StringBuffer();
            sb.append(i2);
            sb.append("/");
            sb.append(i1);
            sb.append("/");
            sb.append(i);
            String str1= sb.toString();
            DateRDV newdate = new DateRDV(db.getUser(),str1);
            db.adddateRDV(newdate);
            Log.v("Added",newdate.get_dispo());
            Toast.makeText(ActivityMeet.this, year_x + " /" + month_x + "/" + day_x, Toast.LENGTH_LONG).show();
        }
    };


}
