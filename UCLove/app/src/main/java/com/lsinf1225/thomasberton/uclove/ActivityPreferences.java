package com.lsinf1225.thomasberton.uclove;


import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.app.Activity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;
import android.content.res.Configuration;
import android.content.res.Resources;
import com.lsinf1225.thomasberton.uclove.database.Compte;
import com.lsinf1225.thomasberton.uclove.database.DatabaseHandler;
import com.lsinf1225.thomasberton.uclove.database.Person;
import com.lsinf1225.thomasberton.uclove.database.Preferences;
import com.lsinf1225.thomasberton.uclove.loginandregister.RegisterActivity;

/**
 * Created by BTL on 3/05/16.
 */
public class ActivityPreferences extends Activity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final DatabaseHandler db = new DatabaseHandler(ActivityPreferences.this);
        setContentView(R.layout.preferences);

    /*    Preferences pref = db.getPreferences(db.getUser());

  /*      CheckBox age = (CheckBox) findViewById(R.id.age);
        age.setChecked(pref.get_visibilityAge());

        CheckBox name = (CheckBox) findViewById(R.id.name);
        name.setChecked(pref.get_visibilityName());

        CheckBox personality = (CheckBox) findViewById(R.id.personality);
        personality.setChecked(pref.get_visibilityPersonality());

        CheckBox description = (CheckBox) findViewById(R.id.description);
        description.setChecked(pref.get_visibilityDescription());

        CheckBox nationality = (CheckBox) findViewById(R.id.nationality);
        nationality.setChecked(pref.get_visibilityNationality());

        Switch language = (Switch) findViewById(R.id.switchLG);
        language.setChecked(pref.get_language()==1);

*/


        Switch switch1 = (Switch) findViewById(R.id.switchLG);
        switch1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {
                    //Toast.makeText(getApplicationContext(), "ON", Toast.LENGTH_LONG).show();
                    // LanguageHelper.changeLocale(this.getRessources(),"en");
                } else {
                    //Toast.makeText(getApplicationContext(), "OFF", Toast.LENGTH_LONG).show();
                }
            }
        });

        Button btnSaveModif = (Button) findViewById(R.id.btnSaveModif);
        TextView profil = (TextView) findViewById(R.id.cancel_modif);

        btnSaveModif.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View arg0){
                Log.d("Insert: ", "lsgkmodgjrdomgj ..");

                EditText oldpassword = (EditText) findViewById(R.id.oldMdp);
                String oldpassword1 = oldpassword.getText().toString();
                Person person = db.getPerson(db.getUser());
                Compte compte = db.getCompte(person.get_login());
                if(oldpassword1.compareTo(compte.get_password())==0){
                    EditText newpassword = (EditText) findViewById(R.id.newMdp);
                    String newpassword1 = newpassword.getText().toString();

                    compte.set_password(newpassword1);


                    db.updateCompte(compte);

                }

            }
        });

    }





}
