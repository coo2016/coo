package com.lsinf1225.thomasberton.uclove.loginandregister;
        import android.app.Activity;
        import android.content.Intent;
        import android.os.Bundle;
        import android.util.Log;
        import android.view.View;
        import android.widget.Button;
        import android.widget.EditText;
        import android.widget.TextView;

        import com.lsinf1225.thomasberton.uclove.MainActivity;
        import com.lsinf1225.thomasberton.uclove.R;
        import com.lsinf1225.thomasberton.uclove.database.Compte;
        import com.lsinf1225.thomasberton.uclove.database.DatabaseHandler;
        import com.lsinf1225.thomasberton.uclove.database.Person;
        import com.lsinf1225.thomasberton.uclove.database.Preferences;
        import com.lsinf1225.thomasberton.uclove.database.User;
        import com.lsinf1225.thomasberton.uclove.loginandregister.RegisterActivity;



public class LoginActivity extends Activity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // setting default screen to login.xml
        setContentView(R.layout.login);


        Button btnConnect = (Button) findViewById(R.id.btnLogin);

        // Listening to register new account link
        btnConnect.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                // Switching to Register screen

                DatabaseHandler db = new DatabaseHandler(LoginActivity.this);
                EditText login1 = (EditText) findViewById(R.id.reg_login);
                String login = login1.getText().toString();
                Log.v("LOGIN", login);
                User user=new User(login);
                Log.v("LOGIN2", user.get_login());


                EditText password1 = (EditText) findViewById(R.id.reg_password);
                String password = password1.getText().toString();

                Compte compte = db.getCompte(login);
                if(compte.get_password().equals(password)){

                    Log.v("Validation", db.getUser());
                    db.updateUser(user);
                    Log.v("Validation", db.getUser());
                    Intent i = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(i);
                }
                else {
                    Log.v("Validation", "NO");
                }

            }
        });

        TextView registerScreen = (TextView) findViewById(R.id.link_to_register);
        // Listening to register new account link
        registerScreen.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                // Switching to Register screen
                Intent i = new Intent(getApplicationContext(), RegisterActivity.class);
                startActivity(i);
            }
        });
    }
}