package com.lsinf1225.thomasberton.uclove.database;

import java.util.ArrayList;

/**
 * Created by Aetheya on 6/05/2016.
 */
public class DateRDV {

    //private variables
    String _login;
    String _dispo;

    // Empty constructor
    public DateRDV(){

    }

    // constructor
    public DateRDV(String login, String dispo){
        this._login = login;
        this._dispo = dispo;
    }


    // getting ID
    public String get_login(){
        return this._login;
    }

    // setting id
    public void set_login(String id){
        this._login = id;
    }

    // getting pass
    public String get_dispo(){
        return this._dispo;
    }

    // setting pass
    public void set_dispo(String dispo){
        this._dispo = dispo;
    }

    public static ArrayList<DateRDV> getAListOfDates() {
        ArrayList<DateRDV> listDate = new ArrayList<DateRDV>();

        listDate.add(new DateRDV("a", "1/2/1995"));
        listDate.add(new DateRDV("a", "4/3/1999"));
        return listDate;
    }


}
