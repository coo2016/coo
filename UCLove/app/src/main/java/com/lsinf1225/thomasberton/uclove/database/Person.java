

/**
 * Created by Aetheya on 2/05/2016.
 */
package com.lsinf1225.thomasberton.uclove.database;

import java.util.ArrayList;

public class Person {

    //private variables
    String _login;
    String _name;
    String _surname;
    String _naissance;
    String _fac;
    String _orientation;
    String _genre;
    String _nationalite;
    String _description;
    String _photo;

    // Empty constructor
    public Person(){

    }

// constructor without description
    public Person(String login, String name, String surname, String naissance, String fac, String orientation, String genre, String nationalite){
        this._login = login;
        this._name = name;
        this._surname = surname;
        this._naissance = naissance;
        this._fac = fac;
        this._orientation= orientation;
        this._genre = genre;
        this._nationalite = nationalite;
    }

    // constructor
    public Person(String login, String name, String surname, String naissance, String fac, String orientation, String genre, String nationalite, String description){
        this._login = login;
        this._name = name;
        this._surname = surname;
        this._naissance = naissance;
        this._fac = fac;
        this._orientation= orientation;
        this._genre = genre;
        this._nationalite = nationalite;
        this._description = description;
    }

    // constructor with photo
    public  Person(String login, String name, String surname, String naissance, String fac, String orientation, String genre, String nationalite, String description,String photo){
        this._login = login;
        this._name = name;
        this._surname = surname;
        this._naissance = naissance;
        this._fac = fac;
        this._orientation= orientation;
        this._genre = genre;
        this._nationalite = nationalite;
        this._description = description;
        this._photo = photo;
    }

    // getting description
    public String get_description(){
        return this._description;
    }

    // setting description
    public void set_description(String description){
        this._description = description;
    }


    // getting nationalite
    public String get_nationalite(){
        return this._nationalite;
    }

    // setting nationalite
    public void set_nationalite(String nationalite){
        this._nationalite = nationalite;
    }

    // getting genre
    public String get_genre(){
        return this._genre;
    }

    // setting genre
    public void set_genre(String genre){
        this._genre = genre;
    }

    // getting orientation
    public String get_orientation(){
        return this._orientation;
    }

    // setting orientation
    public void set_orientation(String orientation){
        this._orientation = orientation;
    }

    // getting ID
    public String get_login(){
        return this._login;
    }

    // setting id
    public void set_login(String id){
        this._login = id;
    }

    // getting name
    public String get_name(){
        return this._name;
    }

    // setting name
    public void set_name(String name){
        this._name = name;
    }

    //getting surname
    public String get_surname(){
        return this._surname;
    }

    // setting surname
    public void set_surname(String surname){
        this._surname = surname;
    }

    //getting fac
    public String get_fac(){ return this._fac; }

    // setting fac
    public void set_fac(String fac){
        this._fac = fac;
    }

    //getting naissance
    public String get_naissance(){
        return this._naissance;
    }

    // setting naissance
    public void set_naissance(String naissance){
        this._naissance = naissance;
    }

    public String get_photo(){ return this._photo;}

    public void set_photo(String photo) {
        this._photo = photo;
    }

    public static ArrayList<Person> getAListOfPersons() {
        ArrayList<Person> listPerson = new ArrayList<Person>();

        listPerson.add(new Person("a", "Sophie", "surname", "naissance", "fac", "orientation", "genre", "nationalite", "description", "photo"));
        listPerson.add(new Person("b", "Valentin", "surname", "naissance", "fac", "orientation", "genre", "nationalite", "description", "photo"));
        listPerson.add(new Person("c", "Thomas", "surname", "naissance", "fac", "orientation", "genre", "nationalite", "description", "photo"));
        listPerson.add(new Person("d", "Baptiste", "surname", "naissance", "fac", "orientation", "genre", "nationalite", "description", "photo"));
        return listPerson;
    }

}