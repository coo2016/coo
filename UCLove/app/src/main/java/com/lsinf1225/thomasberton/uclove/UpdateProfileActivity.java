package com.lsinf1225.thomasberton.uclove;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.lsinf1225.thomasberton.uclove.database.DatabaseHandler;
import com.lsinf1225.thomasberton.uclove.database.DbBitmapUtility;
import com.lsinf1225.thomasberton.uclove.database.Person;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;


public class UpdateProfileActivity extends Activity {
    EditText name1;
    EditText surname1;
    EditText date1;
    EditText fac1;
    EditText genre1;
    EditText orientation1;
    EditText nationalite1;
    EditText description1;
    int REQUEST_CAMERA;
    int SELECT_FILE;
    DatabaseHandler db = new DatabaseHandler(UpdateProfileActivity.this);

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Set View to register.xml
        setContentView(R.layout.activity_update_profile);

        Button btnSaveModif = (Button) findViewById(R.id.btnSaveModif);
        TextView profil = (TextView) findViewById(R.id.cancel_modif);

        profil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("OC_RSS", "retour au profil");
                ImageView imgprofil = (ImageView) findViewById(R.id.imageprofil);
finish();
              //  Intent Intent2 = new Intent(UpdateProfileActivity.this, ActivityProfil.class);
              //  startActivity(Intent2);

            }
        });


        Person person = db.getPerson(db.getUser());//Profil actuel

        //Suggestions de texte
        name1 = (EditText) findViewById(R.id.name2);
        name1.setText(person.get_name(), TextView.BufferType.EDITABLE);

        surname1 = (EditText) findViewById(R.id.surname2);
        surname1.setText(person.get_surname(), TextView.BufferType.EDITABLE);

        date1 = (EditText) findViewById(R.id.date2);
        date1.setText(person.get_naissance(), TextView.BufferType.EDITABLE);

        genre1 = (EditText) findViewById(R.id.genre2);
        genre1.setText(person.get_genre(), TextView.BufferType.EDITABLE);

        orientation1 = (EditText) findViewById(R.id.orientation2);
        orientation1.setText(person.get_orientation(), TextView.BufferType.EDITABLE);

        fac1 = (EditText) findViewById(R.id.fac2);
        fac1.setText(person.get_orientation(), TextView.BufferType.EDITABLE);

        nationalite1 = (EditText) findViewById(R.id.nationalite2);
        nationalite1.setText(person.get_nationalite(), TextView.BufferType.EDITABLE);

        description1 = (EditText) findViewById(R.id.description2);
        description1.setText(person.get_description(), TextView.BufferType.EDITABLE);
        // Listening to Person Screen link
        btnSaveModif.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {

                Log.v("Updateprofil", "yo");

                String name = name1.getText().toString();

                String surname = surname1.getText().toString();

                String date = date1.getText().toString();

                String fac = fac1.getText().toString();

                String genre = genre1.getText().toString();

                String orientation = orientation1.getText().toString();

                String nationalite = nationalite1.getText().toString();

                String description = description1.getText().toString();


                // Inserting Contacts
                Log.d("Insert: ", "Inserting ..");
                db.updatePerson(new Person(db.getUser(), name, surname, date, fac, orientation, genre, nationalite, description));

                // Reading all contacts
                Log.d("Reading: ", "Reading all contacts..");
                List<Person> persons = db.getAllPersons();

                for (Person cn : persons) {
                    String log = "LOGIN: " + cn.get_login() + " ,PASSWORD: " + cn.get_name();
                    // Writing Contacts to log
                    Log.d("Name: ", log);


                }
                // Closing registration screen
                // Switching to Person Screen/closing register screen
                finish();
            }
        });
    }
}