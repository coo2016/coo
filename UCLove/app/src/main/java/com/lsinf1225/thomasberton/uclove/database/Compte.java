

/**
 * Created by Aetheya on 2/05/2016.
 */
package com.lsinf1225.thomasberton.uclove.database;

public class Compte {

    //private variables
    String _login;
    String _password;

    // Empty constructor
    public Compte(){

    }

    // constructor
    public Compte(String login, String password){
        this._login = login;
        this._password = password;
    }



    // getting ID
    public String get_login(){
        return this._login;
    }

    // setting id
    public void set_login(String id){
        this._login = id;
    }

    // getting pass
    public String get_password(){
        return this._password;
    }

    // setting pass
    public void set_password(String password){
        this._password = password;
    }


}