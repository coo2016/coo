package com.lsinf1225.thomasberton.uclove;

import android.content.res.Configuration;
import android.content.res.Resources;
import java.util.Locale;

import com.bumptech.glide.load.engine.Resource;

/**
 * Created by Thomas B on 06-05-16.
 */
public class LanguageHelper {
    public static void changeLocale(Resources res,String locale){
        Configuration config;
        config = new Configuration(res.getConfiguration());

        switch (locale) {
            case "es":
                config.locale = new Locale("es");
                break;
            case "en":
                config.locale =  Locale.ENGLISH;
                break;
            default:
                config.locale = Locale.FRENCH;
                break;
        }
        res.updateConfiguration(config, res.getDisplayMetrics());
    }
}