package com.lsinf1225.thomasberton.uclove;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_main);
        //Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //setSupportActionBar(toolbar);

        Button buttonmessage = (Button) findViewById(R.id.buttonmessage);
        buttonmessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("OC_RSS", "section qui mene a activity message");

                Intent Intent3 = new Intent(MainActivity.this, ActivityFriends.class);
                startActivity(Intent3);

            }
        });

        Button buttonprofil2 = (Button) findViewById(R.id.buttonprofil2);
        buttonprofil2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("OC_RSS", "section qui mene au profil");

                Intent Intent2 = new Intent(MainActivity.this, ActivityProfil.class);
                startActivity(Intent2);

            }
        });
        Button buttonPreference = (Button) findViewById(R.id.button4);
        buttonPreference.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("OC_RSS", "section qui mene aux preferences");

                Intent Intent4 = new Intent(MainActivity.this, ActivityPreferences.class);
                startActivity(Intent4);
            }
        });

        Button buttonCompte = (Button) findViewById(R.id.buttonmeet);
        buttonCompte.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("OC_RSS", "section qui mene au compte");

                Intent Intent5 = new Intent(MainActivity.this, ActivityMeet.class);
                startActivity(Intent5);
            }
        });

        Button buttonresearch = (Button) findViewById(R.id.buttonresearch);
        buttonresearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("OC_RSS", "section qui mene à la recherche");

                Intent Intent2 = new Intent(MainActivity.this, ActivityResearch.class);
                startActivity(Intent2);

            }
        });
    }
    /*
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }
   */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
