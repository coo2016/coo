package com.lsinf1225.thomasberton.uclove;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.lsinf1225.thomasberton.uclove.database.Person;
import com.lsinf1225.thomasberton.uclove.ActivityProfil;
import com.lsinf1225.thomasberton.uclove.database.DatabaseHandler;
import com.lsinf1225.thomasberton.uclove.tindercard.FlingCardListener;
import com.lsinf1225.thomasberton.uclove.tindercard.SwipeFlingAdapterView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by X on 20-04-16.
 */
public class ActivityResearch extends Activity implements FlingCardListener.ActionDownInterface {
    public DatabaseHandler db = new DatabaseHandler(ActivityResearch.this);
    public static MyAppAdapter myAppAdapter;
    public static ViewHolder viewHolder;
    private ArrayList<Data> al;
    private SwipeFlingAdapterView flingContainer;

    public static void removeBackground() {


        viewHolder.background.setVisibility(View.GONE);
        myAppAdapter.notifyDataSetChanged();

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.research_activity);

        Button buttonfriends = (Button) findViewById(R.id.buttonfriends);
        buttonfriends.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("OC_RSS", "section qui mene au profil, preferences et compte");

                Intent Intent = new Intent(ActivityResearch.this, ActivityFriends.class);
                startActivity(Intent);

            }
        });
        Button buttonprofil = (Button) findViewById(R.id.buttonprofil);
        buttonprofil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("OC_RSS", "section qui mene au profil apd research");

                Intent Intent = new Intent(ActivityResearch.this, ActivityProfil.class);
                startActivity(Intent);

            }
        });


        flingContainer = (SwipeFlingAdapterView) findViewById(R.id.frame);
        // La je mets toutes les personnes dans les cartes que l'on swap
       // db.getPerson(Integer.toString(0)).set_photo("http://i.ytimg.com/vi/PnxsTxV8y3g/maxresdefault.jpg");
       // db.getPerson(Integer.toString(1)).set_photo("http://switchboard.nrdc.org/blogs/dlashof/mission_impossible_4-1.jpg");

       // List<Person> liste = db.getAllPersons();
       /* for(int i=0;i<al.size();i++){
            Person person = liste.get(i);
            al.add(new Data(person.get_photo(),person.get_name()));
        }*/

/*
        values.put(KEY_NAME, "Adeline");
        values.put(KEY_IMAGE, "https://scontent-cdg2-1.xx.fbcdn.net/hphotos-xfa1/v/t1.0-9/64480_10200659682755348_136437729_n.jpg?oh=d4a118239423b9e221eedcc46864a5cf&oe=5767F151");


        values.put(KEY_NAME, "Sophie");
        values.put(KEY_IMAGE, "http://zupimages.net/viewer.php?id=16/08/2h0y.jpg");

        values.put(KEY_NAME, "Baptiste");
        values.put(KEY_IMAGE, "https://scontent-cdg2-1.xx.fbcdn.net/hphotos-xat1/t31.0-8/10560553_321275408040100_5606878839619983447_o.jpg");

        values.put(KEY_NAME,"Gregoire");
        values.put(KEY_IMAGE, "http://img.src.ca/2013/06/10/635x357/130610_0t22a_canard-_sn635.jpg");

        values.put(KEY_NAME, "Valou");
        values.put(KEY_IMAGE, "http://imageshack.com/a/img923/8620/hIbbB7.jpg");
        // Inserting Row

        */
        al = new ArrayList<>();



        db.addPerson(new Person("1", "Adeline", "", "", "", "", "", "", "", "https://scontent-cdg2-1.xx.fbcdn.net/hphotos-xfa1/v/t1.0-9/64480_10200659682755348_136437729_n.jpg?oh=d4a118239423b9e221eedcc46864a5cf&oe=5767F151"));

            db.addPerson(new Person("2", "Sophie", "", "", "", "", "", "", "", "http://zupimages.net/viewer.php?id=16/08/2h0y.jpg"));

            db.addPerson(new Person("3", "Baptiste", "", "", "", "", "", "", "", "https://scontent-cdg2-1.xx.fbcdn.net/hphotos-xat1/t31.0-8/10560553_321275408040100_5606878839619983447_o.jpg"));

           db.addPerson(new Person("4", "Gregoire", "", "", "", "", "", "", "", "http://img.src.ca/2013/06/10/635x357/130610_0t22a_canard-_sn635.jpg"));

            db.addPerson(new Person("5", "valou", "", "", "", "", "", "", "", "http://imageshack.com/a/img923/8620/hIbbB7.jpg"));




           ArrayList<Person> liste = new ArrayList<>();
           liste.add(db.getPerson("1"));
        liste.add(db.getPerson("2"));
        liste.add(db.getPerson("3"));
        liste.add(db.getPerson("4"));
        liste.add(db.getPerson("5"));
        for(int i=0; i<liste.size();i++){
            al.add(new Data(liste.get(i).get_photo(), liste.get(i).get_name()));
        }

        db.addIdAime("1",db.getPerson(db.getUser()).get_name());

        db.addIdAime("2",db.getPerson(db.getUser()).get_name());

        db.addIdAime("3",db.getPerson(db.getUser()).get_name());


        db.addIdAime("4",db.getPerson(db.getUser()).get_name());


        db.addIdAime("5",db.getPerson(db.getUser()).get_name());




       /*
        al.add(new Data("http://i.ytimg.com/vi/PnxsTxV8y3g/maxresdefault.jpg", "Thomas"));
        al.add(new Data("http://switchboard.nrdc.org/blogs/dlashof/mission_impossible_4-1.jpg", "Valentin"));
        al.add(new Data("http://i.ytimg.com/vi/PnxsTxV8y3g/maxresdefault.jpg", "Baptiste"));
        */
        /*
        al.add(new Data("http://switchboard.nrdc.org/blogs/dlashof/mission_impossible_4-1.jpg", "But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness."));
        al.add(new Data("http://i.ytimg.com/vi/PnxsTxV8y3g/maxresdefault.jpg", "But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness."));
   */


        myAppAdapter = new MyAppAdapter(al, ActivityResearch.this);
        flingContainer.setAdapter(myAppAdapter);
        flingContainer.setFlingListener(new SwipeFlingAdapterView.onFlingListener() {
            @Override
            public void removeFirstObjectInAdapter() {

            }

            @Override
            public void onLeftCardExit(Object dataObject) {
                al.remove(0);   //suprrime la carte pour eviter qu'elle revienne
                myAppAdapter.notifyDataSetChanged();
                //Do something on the left!
                //You also have access to the original object.
                //If you want to use it just cast it (String) dataObject
               //Data a = al.get(0);

            }


            @Override
            public void onRightCardExit(Object dataObject) {
                String url = al.get(0).getImagePath();

                Person pers = db.getPersonFromUrl(url);
                db.addIdAime(db.getUser(), pers.get_login());

               //  if (db.findE(pers.get_login()) && db.findR(db.getUser())){                      // SI recepiprocité

                     /*Friends friend = new Friends(db.getPerson(db.getUser()), pers);
                     Friends friend2 = new Friends (pers, db.getPerson(db.getUser()));
                     */

                 //}

                al.remove(0);
                myAppAdapter.notifyDataSetChanged();
                //String login = db.getUser();
                //Person person=db.getPerson(login);
                //db.addIdAime(person.get_login(),personne1.get_login());

            }

            @Override
            public void onAdapterAboutToEmpty(int itemsInAdapter) {

            }

            @Override
            public void onScroll(float scrollProgressPercent) {

                View view = flingContainer.getSelectedView();
                view.findViewById(R.id.background).setAlpha(0);
                view.findViewById(R.id.item_swipe_right_indicator).setAlpha(scrollProgressPercent < 0 ? -scrollProgressPercent : 0);
                view.findViewById(R.id.item_swipe_left_indicator).setAlpha(scrollProgressPercent > 0 ? scrollProgressPercent : 0);
            }
        });


        // Optionally add an OnItemClickListener
        flingContainer.setOnItemClickListener(new SwipeFlingAdapterView.OnItemClickListener() {
            @Override
            public void onItemClicked(int itemPosition, Object dataObject) {

                View view = flingContainer.getSelectedView();
                view.findViewById(R.id.background).setAlpha(0);

                myAppAdapter.notifyDataSetChanged();
            }
        });

    }

    @Override
    public void onActionDownPerform() {
        Log.e("action", "bingo");
    }

    public static class ViewHolder {
        public static FrameLayout background;
        public TextView DataText;
        public ImageView cardImage;


    }

    public class MyAppAdapter extends BaseAdapter {


        public List<Data> parkingList;
        public Context context;

        private MyAppAdapter(List<Data> apps, Context context) {
            this.parkingList = apps;
            this.context = context;
        }

        @Override
        public int getCount() {
            return parkingList.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            View rowView = convertView;


            if (rowView == null) {

                LayoutInflater inflater = getLayoutInflater();
                rowView = inflater.inflate(R.layout.item, parent, false);
                // configure view holder
                viewHolder = new ViewHolder();
                viewHolder.DataText = (TextView) rowView.findViewById(R.id.bookText);
                viewHolder.background = (FrameLayout) rowView.findViewById(R.id.background);
                viewHolder.cardImage = (ImageView) rowView.findViewById(R.id.cardImage);
                rowView.setTag(viewHolder);

            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }
            viewHolder.DataText.setText(parkingList.get(position).getDescription() + "");

            Glide.with(ActivityResearch.this).load(parkingList.get(position).getImagePath()).into(viewHolder.cardImage);

            return rowView;
        }
    }

}
