package com.lsinf1225.thomasberton.uclove;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ListView;
import com.lsinf1225.thomasberton.uclove.database.DatabaseHandler;
import com.lsinf1225.thomasberton.uclove.database.DateRDV;
import com.lsinf1225.thomasberton.uclove.database.DispoAdapter;
import com.lsinf1225.thomasberton.uclove.database.Message;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
/**
 * Created by X on 20-04-16.
 */

public class ActivityListMeet extends Activity implements DispoAdapter.DispoAdapterListener {
    DatabaseHandler db = new DatabaseHandler(ActivityListMeet.this);
    DispoAdapter adapter;
    String loginR;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Intent myIntent = getIntent();
        loginR = myIntent.getStringExtra("loginR");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activity_list_meet);

       /*
        //Comparaison des disponibilités et mets les communes dans duo
        List<DateRDV> duo=  new ArrayList<DateRDV>();
        List<DateRDV> listeD = db.getAllRDV(loginR);
        Object [] autre= listeD.toArray();
        List<DateRDV> listeR = db.getAllRDV(db.getUser());
        Object [] moi= listeR.toArray();
        for(int i=0; i<moi.length-1;i++){
            for(int j=0; j<autre.length-1;j++){
                if(moi[i].equals(autre[j])){
                    duo.add((DateRDV)moi[i]);
                }
            }
        }
        */

        ArrayList<DateRDV> duo= DateRDV.getAListOfDates();

        DispoAdapter adapter = new DispoAdapter(this, duo);
        adapter.addListener(this);
        ListView list = (ListView) findViewById(R.id.ListView4);
        list.setAdapter(adapter);
    }

    public void onClickNom(DateRDV item, int position) {

        //RECUPERE LA DATE
        Calendar c = Calendar.getInstance();
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);
        int heure = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);

        StringBuffer datemess = new StringBuffer();
        datemess.append(heure);
        datemess.append(":");
        datemess.append(minute);
        datemess.append(" ");
        datemess.append(day);
        datemess.append("/");
        datemess.append(month);

        String daterecup = datemess.toString();


        Message newMess = new Message(db.getUser(), loginR, daterecup, item.get_dispo());
        db.addMessage(newMess);

        finish();
    }

}
