package com.lsinf1225.thomasberton.uclove.database;

/**
 * Created by BTL on 6/05/16.
 */
public class Preferences {

    //private variable
    String _login;
    int _visibilityAge;
    int _visibilityName;
    int _visibilityPersonality;
    int _visibilityDescription;
    int _visibilityNationality;
    int _language;

    // Empty constructor
    public Preferences(){}

    public Preferences(String login, int visibilityAge, int visibilityName, int visibilityPersonality, int visibilityDescription, int visibilityNationality, int language) {
        this._login = login;
        this._visibilityAge = visibilityAge;
        this._visibilityName = visibilityName;
        this._visibilityPersonality = visibilityPersonality;
        this._visibilityDescription = visibilityDescription;
        this._visibilityNationality = visibilityNationality;
        this._language = language;
    }

    // getting ID
    public String get_login(){
        return this._login;
    }
    //getter
    public boolean get_visibilityAge() { return this._visibilityAge==1; }
    public boolean get_visibilityName() { return this._visibilityName==1; }
    public boolean get_visibilityPersonality() { return this._visibilityPersonality==1; }
    public boolean get_visibilityDescription() { return this._visibilityDescription==1; }
    public boolean get_visibilityNationality() { return this._visibilityNationality==1; }
    public int get_language() { return this._language; }


    //setter
    public void set_visibilityAge(int age) {  this._visibilityAge = age; }
    public void set_visibilityName(int name) {  this._visibilityName = name; }
    public void set_visibilityPersonality(int personality) {  this._visibilityPersonality = personality; }
    public void set_visibilityDescription(int description) {  this._visibilityDescription = description; }
    public void set_visibilityNationality(int nationality) {  this._visibilityNationality = nationality; }
    public void set_language(int language) {  this._language = language; }
    // setting id
    public void set_login(String id){
        this._login = id;
    }

}
