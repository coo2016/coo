package com.lsinf1225.thomasberton.uclove.loginandregister;

        import android.app.Activity;
        import android.content.Intent;
        import android.os.Bundle;
        import android.util.Log;
        import android.view.View;
        import android.widget.Button;
        import android.widget.EditText;
        import android.widget.TextView;

        import com.lsinf1225.thomasberton.uclove.ActivityResearch;
        import com.lsinf1225.thomasberton.uclove.R;
        import com.lsinf1225.thomasberton.uclove.database.Compte;
        import com.lsinf1225.thomasberton.uclove.database.DatabaseHandler;
        import com.lsinf1225.thomasberton.uclove.database.Person;
        import com.lsinf1225.thomasberton.uclove.database.Preferences;

        import java.util.List;


public class RegisterActivity extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Set View to register.xml
        setContentView(R.layout.register);

        TextView loginScreen = (TextView) findViewById(R.id.link_to_login);
        Button btnRegister = (Button) findViewById(R.id.btnRegister);

        loginScreen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("OC_RSS", "retour au menu connexion");
                Intent Intent2 = new Intent(RegisterActivity.this, LoginActivity.class);
                startActivity(Intent2);

            }
        });



        // Listening to Person Screen link
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                // INSCRITPION

                DatabaseHandler db = new DatabaseHandler(RegisterActivity.this);
                Log.v("Inscription", "yo");

                EditText name1 = (EditText) findViewById(R.id.reg_name);
                String name = name1.getText().toString();


                EditText surname1 = (EditText) findViewById(R.id.reg_surname);
                String surname = surname1.getText().toString();

                EditText date1 = (EditText) findViewById(R.id.reg_date);
                String date = date1.getText().toString();

                EditText fac1 = (EditText) findViewById(R.id.reg_fac);
                String fac = fac1.getText().toString();

                EditText login1 = (EditText) findViewById(R.id.reg_login);
                String login = login1.getText().toString();

                EditText mdp1 = (EditText) findViewById(R.id.reg_password);
                String mdp = mdp1.getText().toString();

                EditText genre1 = (EditText) findViewById(R.id.reg_genre);
                String genre = genre1.getText().toString();

                EditText orientation1 = (EditText) findViewById(R.id.reg_orientation);
                String orientation = orientation1.getText().toString();

                EditText nationalite1 = (EditText) findViewById(R.id.reg_nationalite);
                String nationalite = nationalite1.getText().toString();

                EditText description1 = (EditText) findViewById(R.id.reg_description);
                String description = description1.getText().toString();


                // Inserting Contacts
                Log.d("Insert: ", "Inserting ..");
                db.addCompte(new Compte(login, mdp));
                db.addPerson(new Person(login, name, surname, date, fac, orientation, genre, nationalite, description));

                // Reading all contacts
                Log.d("Reading: ", "Reading all contacts..");
                List<Compte> comptes = db.getAllComptes();


                //inserting pref
                db.addPref(new Preferences(login,1,1,1,1,1,1));

                for (Compte cn : comptes) {
                    String log = "LOGIN: " + cn.get_login() + " ,PASSWORD: " + cn.get_password();
                    // Writing Contacts to log
                    Log.d("Name: ", log);


                }


                // Closing registration screen
                // Switching to Person Screen/closing register screen
                finish();
            }
        });
    }
}