package com.lsinf1225.thomasberton.uclove;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.lsinf1225.thomasberton.uclove.database.DatabaseHandler;
import com.lsinf1225.thomasberton.uclove.database.Message;
import com.lsinf1225.thomasberton.uclove.database.MessageAdapter;
import com.lsinf1225.thomasberton.uclove.database.Person;

import java.util.Calendar;
import java.util.List;

/**
 * Created by X on 27-04-16.
 */

public class ActivityMessage extends Activity {


    DatabaseHandler db = new DatabaseHandler(ActivityMessage.this);
    MessageAdapter adapter;
    String loginR;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Intent myIntent = getIntent(); // gets the previously created intent
        loginR = myIntent.getStringExtra("loginR");

        super.onCreate(savedInstanceState);
        setContentView(R.layout.messaging);
        List<Message> messages = db.getAllMessage(db.getUser(),loginR);
       //List<Message> messages = Message.getAListOfMessages();

        //Création et initialisation de l'Adapter pour les personnes
         adapter = new MessageAdapter(this, messages);

        //Récupération du composant ListView
        ListView list = (ListView)findViewById(R.id.ListView01);

        //Initialisation de la liste avec les données
        list.setAdapter(adapter);

        //ENVOI D'UN MESSAGE
        Button envoi = (Button) findViewById(R.id.sendButton);
        envoi.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                EditText envoi = (EditText) findViewById(R.id.messageBodyField);
                String envoi1 = envoi.getText().toString();
                // Switching to Register screen

                //RECUPERE LA DATE
                Calendar c = Calendar.getInstance();
                int month = c.get(Calendar.MONTH);
                int day = c.get(Calendar.DAY_OF_MONTH);
                int heure =c.get(Calendar. HOUR_OF_DAY);
                int minute=c.get(Calendar.MINUTE);

                StringBuffer datemess=new StringBuffer();
                datemess.append(heure);
                datemess.append(":");
                datemess.append(minute);
                datemess.append(" ");
                datemess.append(day);
                datemess.append("/");
                datemess.append(month);

                String daterecup = datemess.toString();
                Message newMess= new Message(db.getUser(),loginR,daterecup ,envoi1);

                db.addMessage(newMess);
                Log.v("Message added:",envoi1);
                reloadAllData();
            }
        });
        Button rdv = (Button) findViewById(R.id.btnrdv);
        rdv.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                Intent intent3 = new Intent(ActivityMessage.this, ActivityListMeet.class);
                intent3.putExtra("loginR",loginR);
                startActivity(intent3);
            }
        });

    }
    private void reloadAllData(){
        // get new modified random data
        List<Message> objects = db.getAllMessage(db.getUser(),loginR);
        // update data in our adapter
        adapter.getData().clear();
        adapter.getData().addAll(objects);
        // fire the event
        adapter.notifyDataSetChanged();
    }
}
