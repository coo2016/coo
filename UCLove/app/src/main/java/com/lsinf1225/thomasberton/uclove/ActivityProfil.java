package com.lsinf1225.thomasberton.uclove;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.lsinf1225.thomasberton.uclove.database.DatabaseHandler;
import com.lsinf1225.thomasberton.uclove.database.Person;

/**
 * Created by X on 27-04-16.
 */
public class ActivityProfil extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profil);

        DatabaseHandler db = new DatabaseHandler(ActivityProfil.this);

        String login = db.getUser();
        Person person = db.getPerson(login);

        TextView prenom = (TextView) findViewById(R.id.prenom);
        TextView nom = (TextView) findViewById(R.id.nom);
        TextView datenaissance = (TextView) findViewById(R.id.textView7);
        TextView description = (TextView) findViewById(R.id.textView8);
        TextView fac = (TextView) findViewById(R.id.textView6);
        TextView genre = (TextView) findViewById(R.id.textView9);
        TextView orientation = (TextView) findViewById(R.id.textView10);

        description.setText(person.get_description());
        fac.setText(person.get_fac());
        genre.setText(person.get_fac());
        orientation.setText(person.get_orientation());
        datenaissance.setText(person.get_naissance());
        prenom.setText(person.get_name());
        nom.setText(person.get_surname());


        Button butmodif = (Button) findViewById(R.id.butmodif);
        // Listening to register new account link
        butmodif.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                // Switching to Register screen
                Intent i = new Intent(getApplicationContext(), UpdateProfileActivity.class);
                startActivity(i);


            }
        });


    }

    public static void maj() {
    }
}
