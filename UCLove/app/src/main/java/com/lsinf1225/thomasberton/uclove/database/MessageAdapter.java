package com.lsinf1225.thomasberton.uclove.database;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.lsinf1225.thomasberton.uclove.R;

import java.util.List;

/**
 * Created by Aetheya on 5/05/2016.
 */
public class MessageAdapter extends BaseAdapter {

    // Une liste de personnes
    private List<Message> mListM;

    //Le contexte dans lequel est présent notre adapter
    private Context mContext;

    //Un mécanisme pour gérer l'affichage graphique depuis un layout XML
    private LayoutInflater mInflater;

    public MessageAdapter(Context context, List<Message> aListM) {
        mContext = context;
        mListM = aListM;
        mInflater = LayoutInflater.from(mContext);
    }
    public int getCount() {
        return mListM.size();
    }
    public Object getItem(int position) {
        return mListM.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        RelativeLayout layoutItem;
        //(1) : Réutilisation des layouts
        if (convertView == null) {
            //Initialisation de notre item à partir du  layout XML "message_left.xml"
            layoutItem = (RelativeLayout) mInflater.inflate(R.layout.message_left, parent, false);
        } else {
            layoutItem = (RelativeLayout) convertView;
        }
        Log.v("Message Adaptater","log1");

        //(2) : Récupération des TextView de notre layout
        TextView sender = (TextView)layoutItem.findViewById(R.id.textfriend);
        TextView datem = (TextView)layoutItem.findViewById(R.id.txtDate);
        TextView txtmess = (TextView)layoutItem.findViewById(R.id.txtMessage);
        Log.v("Message Adaptater","log2");
        //(3) : Renseignement des valeurs
        sender.setText(mListM.get(position)._loginE);
        datem.setText(mListM.get(position)._dateM);
        txtmess.setText(mListM.get(position)._content);
        Log.v("Message Adaptater",mListM.get(position)._loginE);
        Log.v("Message Adaptater",mListM.get(position)._dateM);
        Log.v("Message Adaptater",mListM.get(position)._content);
        //On retourne l'item créé.
        return layoutItem;
    }


    public List<Message> getData() {
        return mListM;
    }
}
