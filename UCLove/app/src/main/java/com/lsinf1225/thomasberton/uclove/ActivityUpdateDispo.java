package com.lsinf1225.thomasberton.uclove;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ListView;
import com.lsinf1225.thomasberton.uclove.database.DatabaseHandler;
import com.lsinf1225.thomasberton.uclove.database.DateRDV;
import com.lsinf1225.thomasberton.uclove.database.DispoAdapter;
import com.lsinf1225.thomasberton.uclove.database.Message;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
/**
 * Created by X on 20-04-16.
 */

public class ActivityUpdateDispo extends Activity implements DispoAdapter.DispoAdapterListener {
    DatabaseHandler db = new DatabaseHandler(ActivityUpdateDispo.this);
    DispoAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activity_list_meet);

        //Comparaison des disponibilités et mets les communes dans duo
        List<DateRDV> listeD = db.getAllRDV(db.getUser());

        DispoAdapter adapter = new DispoAdapter(this, listeD);
        adapter.addListener(this);
        ListView list = (ListView) findViewById(R.id.ListView4);
        list.setAdapter(adapter);
    }

    public void onClickNom(DateRDV item, int position) {

        db.deleteDate(db.getUser(),item.get_dispo());
        finish();
    }

}
