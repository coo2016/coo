package com.lsinf1225.thomasberton.uclove;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.lsinf1225.thomasberton.uclove.database.DatabaseHandler;
import com.lsinf1225.thomasberton.uclove.database.FriendAdaptater;
import com.lsinf1225.thomasberton.uclove.database.Message;
import com.lsinf1225.thomasberton.uclove.database.MessageAdapter;
import com.lsinf1225.thomasberton.uclove.database.Person;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by X on 20-04-16.
 */


public class ActivityFriends extends Activity implements FriendAdaptater.PersonAdapterListener {

    DatabaseHandler db = new DatabaseHandler(ActivityFriends.this);
    FriendAdaptater adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.friends);

        //List<Person> amis = db.getAllFriends(db.getUser());
        ArrayList<Person> listP = Person.getAListOfPersons();
        FriendAdaptater adapter = new FriendAdaptater(this, listP);

        //Ecoute des évènements sur votre liste
        adapter.addListener(this);

        ListView list = (ListView) findViewById(R.id.ListView);

        list.setAdapter(adapter);


    }

    public void onClickNom(Person item, int position) {


        Intent intent3 = new Intent(ActivityFriends.this, ActivityMessage.class);
Log.v("LOGINR",item.get_login());
        Log.v("LOGINAME",item.get_name());
        intent3.putExtra("loginR",item.get_login());

        startActivity(intent3);

        //AlertDialog.Builder builder = new AlertDialog.Builder(this);
       // builder.setTitle("Personne");

       // builder.setMessage("Vous avez cliqué sur : " + item.get_name());
       // builder.setPositiveButton("Oui", null);
       // builder.setNegativeButton("Non", null);
       // builder.show();
    }

}
